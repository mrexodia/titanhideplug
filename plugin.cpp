#include "plugin.h"
#include <windows.h>
#include <stdio.h>
#include "TitanHide.h"

static DWORD pid=0;
static bool hidden=false;

static void TitanHideCall(HIDE_COMMAND Command)
{
    HANDLE hDevice=CreateFileA("\\\\.\\TitanHide", GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
    if(hDevice==INVALID_HANDLE_VALUE)
    {
        _plugin_logputs("[TITANHIDE] could not open TitanHide handle");
        return;
    }
    HIDE_INFO HideInfo;
    HideInfo.Command=Command;
    HideInfo.Pid=pid;
    HideInfo.Type=0xFFFFFFFF; //everything TODO: update
    DWORD written=0;
    if(WriteFile(hDevice, &HideInfo, sizeof(HIDE_INFO), &written, 0))
    {
        _plugin_logputs("[TITANHIDE] process hidden!");
        hidden=true;
    }
    else
        _plugin_logputs("[TITANHIDE] WriteFile error...");
    CloseHandle(hDevice);
}

static void cbCreateProcess(CBTYPE cbType, void* callbackInfo)
{
    PLUG_CB_CREATEPROCESS* info=(PLUG_CB_CREATEPROCESS*)callbackInfo;
    pid=info->fdProcessInfo->dwProcessId;
}

static void cbStopDebug(CBTYPE cbType, void* callbackInfo)
{
    if(hidden)
    {
        hidden=false;
        _plugin_logprintf("[TITANHIDE] unhiding %X (%ud)\n", pid, pid);
        TitanHideCall(UnhidePid);
    }
}

static bool cbTitanHide(int argc, char* argv[])
{
    _plugin_logprintf("[TITANHIDE] hiding %X (%ud)\n", pid, pid);
    TitanHideCall(HidePid);
    if(argc>1) //also do the TitanEngine hiding
        DbgCmdExec("hide");
    return true;
}

void TitanHideInit(PLUG_INITSTRUCT* initStruct)
{
    _plugin_registercallback(pluginHandle, CB_CREATEPROCESS, cbCreateProcess);
    _plugin_registercallback(pluginHandle, CB_STOPDEBUG, cbStopDebug);
    if(!_plugin_registercommand(pluginHandle, "TitanHide", cbTitanHide, false))
        puts("[TITANHIDE] error registering command!");
}

void TitanHideStop()
{
    _plugin_unregistercallback(pluginHandle, CB_CREATEPROCESS);
    _plugin_unregistercallback(pluginHandle, CB_STOPDEBUG);
    _plugin_unregistercommand(pluginHandle, "TitanHide");
}
